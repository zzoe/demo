package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitee.com/zzoe/demo/client/web/backend/routes"
	"golang.org/x/net/http2"
)

func main() {
	var (
		httpsAddr = flag.String("https_addr", ":4430", "TLS address to listen on ('host:port' or ':port'). Required.")
		httpAddr  = flag.String("http_addr", ":9000", "Plain HTTP address to listen on ('host:port', or ':port'). Empty means no HTTP.")
	)

	flag.Parse()

	httpSrv := &http.Server{Addr: *httpAddr, Handler: nil}
	httpsSrv := &http.Server{Addr: *httpsAddr}

	routes.RegisterHandlers(*httpsAddr)

	// http
	go func() {
		log.Println("http listening on " + *httpAddr)
		if err := httpSrv.ListenAndServe(); err != nil {
			log.Printf("listen: %s\n", err)
		}
	}()

	// https
	go func() {
		log.Println("https listening on " + *httpsAddr)
		http2.ConfigureServer(httpsSrv, nil)
		if err := httpsSrv.ListenAndServeTLS("localhost.crt", "cert.key.pem"); err != nil {
			log.Printf("listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 8 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Web Servers ...")

	ctx, cancel := context.WithTimeout(context.Background(), 8*time.Second)
	defer cancel()
	if err := httpSrv.Shutdown(ctx); err != nil {
		log.Fatal("Http Server Shutdown err: ", err)
	}

	ctx, cancel = context.WithTimeout(context.Background(), 8*time.Second)
	defer cancel()
	if err := httpsSrv.Shutdown(ctx); err != nil {
		log.Fatal("Https Server Shutdown err: ", err)
	}
	log.Println("All Servers Shutdown!")
}
