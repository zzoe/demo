package clientpool

import (
	"fmt"
	"sync"

	"github.com/smallnest/rpcx/client"
)

var (
	xClients sync.Map
)

func init() {
	newClient("Pub")
}

//GetXClient 根据servicePath取xClient
func GetXClient(servicePath string) (xc client.XClient, err error) {
	defer func() {
		if e := recover(); e != nil {
			if ee, ok := e.(error); ok {
				err = ee
				return
			}

			err = fmt.Errorf("failed to get xclient: %v", e)
		}
	}()

	v, ok := xClients.Load(servicePath)
	if ok {
		xc = v.(client.XClient)
	} else {
		xc = newClient(servicePath)
	}

	return
}

func newClient(servicePath string) (xc client.XClient) {
	sd := client.NewEtcdDiscovery("/demo", servicePath, []string{"localhost:2379"}, nil)
	xc = client.NewXClient(servicePath, client.Failover, client.RoundRobin, sd, client.DefaultOption)
	xClients.Store(servicePath, xc)
	return
}
