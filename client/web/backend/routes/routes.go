package routes

import (
	"log"
	"net/http"
	"os"
	"strings"

	"gitee.com/zzoe/demo/client/web/backend/http2rpc"
	"github.com/smallnest/rpcx/codec"
)

// RegisterHandlers register handlers
func RegisterHandlers(httpsAddr string) {
	mux := http.NewServeMux()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.TLS == nil {
			http.Redirect(w, r, httpsHost(httpsAddr), http.StatusFound)
			return
		}
		mux.ServeHTTP(w, r)
	})

	mux.Handle("/rpc", &http2rpc.HTTP2RPC{HTTPCodec: codec.JSONCodec{}})

	mux.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "static/icon/favicon.ico")
	})

	mux.Handle("/node_modules/", http.StripPrefix("/node_modules/", http.FileServer(http.Dir("frontend/node_modules/"))))

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		filename := "frontend/src" + r.URL.Path
		if _, err := os.Stat(filename); os.IsNotExist(err) {
			log.Println("file", filename, "does not exist")
			r.URL.Path = "/"
		}

		http.FileServer(http.Dir("frontend/src/")).ServeHTTP(w, r)
	})
}

func httpsHost(url string) string {
	if strings.HasPrefix(url, ":") {
		return "https://localhost" + url + "/"
	}

	return "https://" + url + "/"
}
