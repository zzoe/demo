package srvdefine

import (
	"errors"
	"fmt"
	"log"
	"reflect"
	"sync"
)

//ServiceMethodParamPool service method param pool
var ServiceMethodParamPool = &SrvVarPool{}

//ParamType param type
type ParamType bool

const (
	//ParamArgs args type
	ParamArgs = ParamType(true)
	//ParamReply reply type
	ParamReply = ParamType(false)
)

//parameter 参数类
type parameter struct {
	Args  reflect.Type
	Reply reflect.Type
}

//SrvVarPool service method param variable pool
type SrvVarPool struct {
	srvMethodMap   map[string]parameter
	argsReplyPools *typePools
}

func init() {
	ServiceMethodParamPool.register(Pub{})
}

//Register rigister all the args and reply of service methods
func (sv *SrvVarPool) register(sysDefine interface{}) {
	sv.srvMethodMap = make(map[string]parameter)
	sv.argsReplyPools = &typePools{
		pools: make(map[reflect.Type]*sync.Pool),
		New: func(t reflect.Type) interface{} {
			var argv reflect.Value

			if t.Kind() == reflect.Ptr { // reply must be ptr
				argv = reflect.New(t.Elem())
			} else {
				argv = reflect.New(t)
			}

			return argv.Interface()
		},
	}

	sysType := reflect.TypeOf(sysDefine)
	sysName := sysType.Name()

	for m := 0; m < sysType.NumMethod(); m++ {
		method := sysType.Method(m)
		mName := method.Name
		mType := method.Type

		// Method needs 2 parameters: *args, *reply.
		if mType.NumIn() != 3 {
			log.Println("method", mName, "has wrong number of parameters:", mType.NumIn())
			continue
		}

		argType := mType.In(1).Elem()
		replyType := mType.In(2).Elem()

		sv.srvMethodMap[sysName+mName] = parameter{argType, replyType}
		fmt.Printf("func (%s) %s(*%s, *%s)\n", sysName, mName, argType, replyType)

		sv.argsReplyPools.Init(argType)
		sv.argsReplyPools.Init(replyType)
	}
}

//Get get param interface{} from the pool
func (sv *SrvVarPool) Get(path string, method string, typ ParamType) (x interface{}, err error) {
	pt, err := sv.getType(path, method, typ)
	if err != nil {
		return
	}

	x = sv.argsReplyPools.Get(pt)
	return
}

//Put put param interface{} back to the pool
func (sv *SrvVarPool) Put(path string, method string, typ ParamType, x interface{}) (err error) {
	pt, err := sv.getType(path, method, typ)
	if err != nil {
		return
	}

	sv.argsReplyPools.Put(pt, x)
	return
}

func (sv *SrvVarPool) getType(path string, method string, typ ParamType) (pt reflect.Type, err error) {
	param := sv.srvMethodMap[path+method]

	if typ == ParamArgs {
		pt = param.Args
	} else {
		pt = param.Reply
	}

	if pt == nil {
		errStr := fmt.Sprintf("The method[%s] of service[%s] has not defined yet!\n", method, path)
		log.Println(errStr)
		err = errors.New(errStr)
		return
	}

	return
}
