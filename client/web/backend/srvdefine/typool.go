package srvdefine

import (
	"reflect"
	"sync"
)

// Reset defines Reset method for pooled object.
type Reset interface {
	Reset()
}

// this struct is not gororutine-safe.
type typePools struct {
	pools map[reflect.Type]*sync.Pool
	New   func(t reflect.Type) interface{}
}

func (p *typePools) Init(t reflect.Type) {
	tp := &sync.Pool{}
	tp.New = func() interface{} {
		return p.New(t)
	}
	p.pools[t] = tp
}

func (p *typePools) Put(t reflect.Type, x interface{}) {
	if o, ok := x.(Reset); ok {
		o.Reset()
	}
	p.pools[t].Put(x)
}

func (p *typePools) Get(t reflect.Type) interface{} {
	return p.pools[t].Get()
}
