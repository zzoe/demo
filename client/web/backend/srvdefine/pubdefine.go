package srvdefine

import (
	"gitee.com/zzoe/demo/message/pubmsg"
)

//Pub Pub服务定义
type Pub struct {
}

//HeroGetAll 取所有英雄数据
func (Pub) HeroGetAll(*pubmsg.HeroGetAllArgs, *pubmsg.HeroGetAllReply) {}

//SignUp 注册
func (Pub) SignUp(*pubmsg.SignArgs, *pubmsg.SignReply) {}

//SignIn 登录
func (Pub) SignIn(*pubmsg.SignArgs, *pubmsg.SignReply) {}
