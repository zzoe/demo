package http2rpc

import (
	"context"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"gitee.com/zzoe/demo/client/web/backend/clientpool"
	"gitee.com/zzoe/demo/client/web/backend/srvdefine"
	"gitee.com/zzoe/demo/message/pubmsg"
	"github.com/smallnest/rpcx/codec"
	"github.com/smallnest/rpcx/share"
	"github.com/vmihailenco/msgpack"
)

const (
	//XServicePath service path
	XServicePath = "X-RPCX-ServicePath"
	//XServiceMethod service method
	XServiceMethod = "X-RPCX-ServiceMethod"
)

//HTTP2RPC http to rpc handler
type HTTP2RPC struct {
	HTTPCodec codec.Codec
}

//ServeHTTP 将http请求转为rpc
func (handler *HTTP2RPC) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//从Cookie中取usercode,sessionid
	userBaseInfo := pubmsg.SignData{}
	for _, v := range r.Cookies() {
		switch v.Name {
		case "usercode":
			userBaseInfo.Usercode = v.Value
			if userBaseInfo.SessionID != "" {
				break
			}
		case "sessionid":
			userBaseInfo.SessionID = v.Value
			if userBaseInfo.Usercode != "" {
				break
			}
		}
	}
	var userBaseInfoBuilder strings.Builder
	userBytes, _ := msgpack.Marshal(userBaseInfo)
	userBaseInfoBuilder.Write(userBytes)

	//http请求转换为rpc请求
	rh := r.Header
	sp := rh.Get(XServicePath)
	sm := rh.Get(XServiceMethod)

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("ioutil.ReadAll failed!")
		response(w, r, nil, err)
		return
	}
	log.Println("request:", string(reqBody))

	//map的并发读是没有问题的！
	args, err := srvdefine.ServiceMethodParamPool.Get(sp, sm, srvdefine.ParamArgs)
	if err != nil {
		log.Printf("srvdefine.SrvVarPool.Get(%s, %s, %v) failed!", sp, sm, srvdefine.ParamArgs)
		response(w, r, nil, err)
		return
	}
	reply, err := srvdefine.ServiceMethodParamPool.Get(sp, sm, srvdefine.ParamReply)
	if err != nil {
		log.Printf("srvdefine.SrvVarPool.Get(%s, %s, %v) failed!", sp, sm, srvdefine.ParamReply)
		response(w, r, nil, err)
		return
	}

	//根据渠道的SerialType解码请求
	if err := handler.HTTPCodec.Decode(reqBody, args); err != nil {
		log.Println("handler.HTTPCodec.Decode failed!")
		response(w, r, nil, err)
		return
	}
	log.Printf("args:%+v\n", args)

	// 取xClient
	xClient, err := clientpool.GetXClient(sp)
	if err != nil {
		log.Println("clientpool.GetXClient failed!")
		response(w, r, nil, err)
		return
	}

	//session校验数据赋值
	xClient.Auth(userBaseInfoBuilder.String())
	ctx := context.WithValue(context.Background(), share.ReqMetaDataKey, make(map[string]string))

	// 开始远程调用
	if err := xClient.Call(ctx, sm, args, reply); err != nil {
		log.Printf("failed to call: %v, %+v, %+v\n", sm, args, reply)
		response(w, r, nil, err)
		return
	}
	log.Printf("reply:%+v\n", reply)
	srvdefine.ServiceMethodParamPool.Put(sp, sm, srvdefine.ParamArgs, args)

	//设置Cookie
	var resUsercode, resSessionID string
	if signReply, ok := reply.(*pubmsg.SignReply); ok && signReply.Body != nil {
		resUsercode = signReply.Body.Usercode
		resSessionID = signReply.Body.SessionID
	}
	// resSeesionIDValue := reflect.ValueOf(reply).MethodByName("GetSessionID").Call(nil)[0]
	// resSessionID := resSeesionIDValue.String()
	log.Println("resUsercode:", resUsercode)
	log.Println("resSessionID:", resSessionID)
	if resUsercode != "" {
		http.SetCookie(w, &http.Cookie{
			Name:    "usercode",
			Value:   resUsercode,
			Expires: time.Now().Add(time.Hour),
		})
	}
	if resSessionID != "" {
		http.SetCookie(w, &http.Cookie{
			Name:    "sessionid",
			Value:   resSessionID,
			Expires: time.Now().Add(time.Hour),
		})
	}

	//根据渠道的SerialType编码请求
	resBody, err := handler.HTTPCodec.Encode(reply)
	if err != nil {
		log.Println("handler.HTTPCodec.Encode failed!")
		response(w, r, nil, err)
	}
	srvdefine.ServiceMethodParamPool.Put(sp, sm, srvdefine.ParamReply, reply)

	// 响应http请求
	response(w, r, resBody, nil)
}

func response(w http.ResponseWriter, r *http.Request, resByte []byte, err error) {
	if err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}

	log.Println("Response:", string(resByte))
	w.Write(resByte)
}
