import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Hero } from './hero';
import { HEROES } from './mock-heroes';

@Injectable()
export class HeroService {
    private url = 'rpc';  // URL to web api
    private headers = new Headers({ 'Content-Type': 'application/json' });

    constructor(private http: Http) { }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    getMockHeroes(): Promise<Hero[]> {
        return new Promise(resolve => {
            // Simulate server latency with 2 second delay
            setTimeout(() => resolve(HEROES), 2000);
        });
    }

    getHeroes(): Promise<Hero[]> {
        let header = new Headers({
            'Content-Type': 'application/json',
            'X-RPCX-ServicePath': 'Pub',
            'X-RPCX-ServiceMethod': 'HeroGetAll'
        });
        let postData = {};
        return this.http.post(this.url, postData, { headers: header })
            .toPromise()
            .then(response => response.json().body.heroes as Hero[])
            .catch(this.handleError);
    }

    getHero(id: number): Promise<Hero> {
        // return this.getHeroes()
        //     .then(heroes => heroes.find(hero => hero.id === id));
        let header = new Headers({
            'Content-Type': 'application/json',
            'X-RPCX-ServicePath': 'Pub',
            'X-RPCX-ServiceMethod': 'SignUp'
        });
        let postData = { Usercode: 'zoe', Password: 'pass' };
        this.http.post(this.url, postData, { headers: header })
            .toPromise()
            .then(response => {
                console.log(response.json().Body.SessionID);
            })
            .catch(this.handleError);
        return new Promise(resolve => {
            // Simulate server latency with 2 second delay
            setTimeout(() => resolve(HEROES[0]));
        });
    }

    update(hero: Hero): Promise<Hero> {
        let postData = {
            head: {
                service: 'Ams',
                method: 'Ams000003'
            },
            body: {
                hero: hero
            }
        };

        return this.http
            .post(this.url, postData, { headers: this.headers })
            .toPromise()
            .then(() => hero)
            .catch(this.handleError);
    }

    create(name: string): Promise<Hero> {
        let postData = {
            head: {
                service: 'Ams',
                method: 'Ams000002'
            },
            body: {
                name: name
            }
        };

        return this.http
            .post(this.url, JSON.stringify(postData), { headers: this.headers })
            .toPromise()
            .then(res => res.json().body.hero as Hero)
            .catch(this.handleError);
    }
}
