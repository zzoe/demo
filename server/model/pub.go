package model

import (
	"github.com/gomodule/redigo/redis"
	"github.com/jinzhu/gorm"
)

//App service
type App struct {
	DB        *gorm.DB
	RedisPool *redis.Pool
}
