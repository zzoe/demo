package model

import (
	"context"
	"log"

	"gitee.com/zzoe/demo/message/pubmsg"
	"gitee.com/zzoe/demo/server/constans"
	"gitee.com/zzoe/demo/server/table"
	"github.com/smallnest/rpcx/share"
)

//HeroGetAll 取英雄信息
func (app *App) HeroGetAll(ctx context.Context, args *pubmsg.HeroGetAllArgs, res *pubmsg.HeroGetAllReply) (err error) {
	log.Printf("args: %+v\n", args)
	//校验
	//通过MetaData取到当前用户代码
	reqMetaData := ctx.Value(share.ReqMetaDataKey).(map[string]string)
	log.Println(reqMetaData["usercode"])
	//取数
	var heros []table.Hero
	app.DB.Find(&heros)
	//处理
	data := &pubmsg.HeroGetAllData{}
	for _, row := range heros {
		//根据业务需求，对响应字段赋值
		repHero := pubmsg.Hero{}
		repHero.ID = row.ID
		repHero.Name = row.Name

		data.Heroes = append(data.Heroes, repHero)
	}
	//响应
	res.Head = &constans.SucNormal
	res.Body = data

	log.Printf("res.Head: %+v\n", res.Head)
	log.Printf("res.Body: %+v\n", res.Body)
	return
}
