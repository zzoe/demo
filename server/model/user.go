package model

import (
	"context"
	"fmt"
	"log"

	"golang.org/x/crypto/bcrypt"

	"gitee.com/zzoe/demo/message/pubmsg"
	"gitee.com/zzoe/demo/server/constans"
	"gitee.com/zzoe/demo/server/table"
	"gitee.com/zzoe/demo/server/util"
)

//SignUp sign up
func (app *App) SignUp(ctx context.Context, args *pubmsg.SignArgs, res *pubmsg.SignReply) (err error) {
	log.Printf("args: %+v\n", args)
	defer func(*pubmsg.SignReply) {
		log.Printf("res.Head: %+v\n", res.Head)
		log.Printf("res.Body: %+v\n", res.Body)
	}(res)

	//非空校验
	if util.IsSpace(args.Usercode) {
		res.Head = &constans.ErrNull
		res.Head.RetMsg = fmt.Sprintf(res.Head.RetMsg, "用户名")
		return
	}
	if util.IsSpace(args.Password) {
		res.Head = &constans.ErrNull
		res.Head.RetMsg = fmt.Sprintf(res.Head.RetMsg, "密码")
		return
	}

	//业务校验
	user := table.User{Usercode: args.Usercode}
	if !app.DB.Where(&user).Find(&user).RecordNotFound() {
		res.Head = &constans.ErrAlredySignUp
		return
	}

	//密码加密
	encodePW, err := bcrypt.GenerateFromPassword([]byte(args.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		res.Head = &constans.ErrSignUpFail
		return
	}

	//生成sessionid
	data := &pubmsg.SignData{Usercode: user.Usercode}
	app.DB.Raw("SELECT gen_random_uuid()").Row().Scan(&data.SessionID)
	if errs := app.DB.GetErrors(); len(errs) > 0 || len(data.SessionID) == 0 {
		log.Println(errs)
		res.Head = &constans.ErrSignUpFail
		return
	}

	//将session信息存到redis
	rc := app.RedisPool.Get()
	defer rc.Close()
	_, err = rc.Do("SET", user.Usercode, data.SessionID, "EX", "1800")
	if err != nil {
		log.Println("redis set failed:", err)
		res.Head = &constans.ErrSignUpFail
		return
	}

	//存储用户信息
	user.Password = string(encodePW)
	app.DB.Create(&user)

	//响应
	res.Head = &constans.SucNormal
	res.Body = data

	return
}

//SignIn sign in
func (app *App) SignIn(ctx context.Context, args *pubmsg.SignArgs, res *pubmsg.SignReply) (err error) {
	log.Printf("args: %+v\n", args)
	defer func(*pubmsg.SignReply) {
		log.Printf("res.Head: %+v\n", res.Head)
		log.Printf("res.Body: %+v\n", res.Body)
	}(res)

	//非空校验
	if util.IsSpace(args.Usercode) {
		res.Head = &constans.ErrNull
		res.Head.RetMsg = fmt.Sprintf(res.Head.RetMsg, "用户名")
		return
	}
	if util.IsSpace(args.Password) {
		res.Head = &constans.ErrNull
		res.Head.RetMsg = fmt.Sprintf(res.Head.RetMsg, "密码")
		return
	}

	//业务校验
	user := table.User{Usercode: args.Usercode}
	app.DB.Where(&user).Find(&user)
	if errs := app.DB.GetErrors(); len(errs) > 0 {
		log.Println(errs)
		res.Head = &constans.ErrIvalidUsercode
		return
	}

	//密码验证
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(args.Password))
	if err != nil {
		log.Println(err)
		res.Head = &constans.ErrIvalidPassword
		return
	}

	//生成sessionid
	data := &pubmsg.SignData{Usercode: user.Usercode}
	app.DB.Raw("SELECT gen_random_uuid()").Row().Scan(&data.SessionID)
	if errs := app.DB.GetErrors(); len(errs) > 0 || len(data.SessionID) == 0 {
		log.Println(errs)
		res.Head = &constans.ErrSignInFail
		return
	}

	//将session信息存到redis
	rc := app.RedisPool.Get()
	defer rc.Close()
	_, err = rc.Do("SET", user.Usercode, data.SessionID, "EX", "1800")
	if err != nil {
		log.Println("redis set failed:", err)
		res.Head = &constans.ErrSignInFail
		return
	}

	//响应
	res.Head = &constans.SucNormal
	res.Body = data

	return
}
