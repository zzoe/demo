// go run -tags etcd server.go
package main

import (
	"context"
	"errors"
	"flag"
	"log"
	"os"
	"os/signal"
	"time"

	"gitee.com/zzoe/demo/message/pubmsg"
	"gitee.com/zzoe/demo/server/model"

	"github.com/gomodule/redigo/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	metrics "github.com/rcrowley/go-metrics"
	"github.com/smallnest/rpcx/protocol"
	"github.com/smallnest/rpcx/server"
	"github.com/smallnest/rpcx/serverplugin"
	"github.com/vmihailenco/msgpack"
)

var (
	pubAddr   = flag.String("pubAddr", "localhost:8100", "pub server address")
	etcdAddr  = flag.String("etcdAddr", "localhost:2379", "etcd address")
	redisAddr = flag.String("redisAddr", "localhost:6379", "redis address")
	cockAddr  = flag.String("cockAddr", "postgresql://zoe@localhost:26257/mld?sslmode=disable", "cockroachdb address")
	basePath  = flag.String("base", "/demo", "prefix path")
	redisPool *redis.Pool
)

func main() {
	flag.Parse()

	// Connect to the "mld" database as the "zoe" user.
	cockDB, err := gorm.Open("postgres", *cockAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer cockDB.Close()

	// Connect to redis
	redisPool = &redis.Pool{
		IdleTimeout: 3 * time.Minute,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", *redisAddr)
			if err != nil {
				return nil, err
			}
			// 选择db
			c.Do("SELECT", 0)
			return c, nil
		},
	}

	pubServer := server.NewServer()

	go createServer(pubServer, "Pub", &model.App{DB: cockDB, RedisPool: redisPool}, *pubAddr)

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 8 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown RPC Servers ...")

	closeDone := make(chan struct{})

	go func() {
		pubServer.Close()
		log.Println("pubServer closed!")
		close(closeDone)
	}()

	select {
	case <-closeDone:
		log.Println("All Servers closed!")
	case <-time.After(8 * time.Second):
		log.Println("Servers close timeout, force shutdown！")
		os.Exit(0)
	}
}

func createServer(s *server.Server, sysName string, sysStruct interface{}, addr string) {
	addRegistryPlugin(s, addr)
	s.RegisterName(sysName, sysStruct, "")
	s.AuthFunc = auth
	s.Serve("tcp", addr)
}

func addRegistryPlugin(s *server.Server, addr string) {
	r := &serverplugin.EtcdRegisterPlugin{
		ServiceAddress: "tcp@" + addr,
		EtcdServers:    []string{*etcdAddr},
		BasePath:       *basePath,
		Metrics:        metrics.NewRegistry(),
		UpdateInterval: 30 * time.Second,
	}

	if err := r.Start(); err != nil {
		log.Fatal(err)
	}

	s.Plugins.Add(r)
}

func auth(ctx context.Context, req *protocol.Message, token string) error {
	if req.ServicePath == "Pub" && (req.ServiceMethod == "SignUp" || req.ServiceMethod == "SignIn") {
		return nil
	} else if token != "" {
		rc := redisPool.Get()
		defer rc.Close()

		userBaseInfo := &pubmsg.SignData{}
		if err := msgpack.Unmarshal([]byte(token), userBaseInfo); err != nil {
			return err
		}

		log.Printf("token:%+v\n", userBaseInfo)
		sessionid, err := redis.String(rc.Do("GET", userBaseInfo.Usercode))
		if err != nil {
			return err
		}

		if sessionid == userBaseInfo.SessionID {
			req.Metadata["usercode"] = userBaseInfo.Usercode
			_, err = rc.Do("SET", userBaseInfo.Usercode, sessionid, "EX", "1800")
			return err
		}
		log.Println("sessionid:", sessionid)
	}

	return errors.New("invalid token")
}
