package table

import (
	"github.com/jinzhu/gorm"
)

//Account account struct
type Account struct {
	gorm.Model
	Usercode string `gorm:"not null"`
	status string
}
