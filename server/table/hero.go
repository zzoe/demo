package table

//Hero hero struct
type Hero struct {
	ID   int `gorm:"primary_key"`
	Name string
}

//TableName Set table name
func (Hero) TableName() string {
	return "pub_hero_info"
}
