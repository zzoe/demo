package table

import (
	"github.com/jinzhu/gorm"
)

//User user struct
type User struct {
	gorm.Model
	Usercode string `gorm:"not null;unique_index"`
	Password string
	Username string
	Email    string
	Mobile   uint
	Address  string
	Accounts []Account
}
