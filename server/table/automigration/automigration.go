package main

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"gitee.com/zzoe/demo/server/table"
)

func main() {
	// Connect to the "mld" database as the "zoe" user.
	const addr = "postgresql://zoe@localhost:26257/mld?sslmode=disable"
	db, err := gorm.Open("postgres", addr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.DropTableIfExists(&table.User{})
	db.AutoMigrate(&table.Hero{}, &table.User{})
}
