package constans

import "gitee.com/zzoe/demo/message"

var (
	//SucNormal 交易成功
	SucNormal = message.ResHead{RetCode: "0000", RetMsg: "交易成功"}
	//ErrNull 字段不允许为空！
	ErrNull = message.ResHead{RetCode: "0001", RetMsg: "%s不能为空！"}
	//ErrAlredySignUp 用户名被占用！
	ErrAlredySignUp = message.ResHead{RetCode: "0002", RetMsg: "用户名被占用！"}
	//ErrSignUpFail 注册失败！
	ErrSignUpFail = message.ResHead{RetCode: "0003", RetMsg: "注册失败！"}
	//ErrIvalidUsercode 无此用户!
	ErrIvalidUsercode = message.ResHead{RetCode: "0004", RetMsg: "无此用户!"}
	//ErrIvalidPassword 密码错误!
	ErrIvalidPassword = message.ResHead{RetCode: "0005", RetMsg: "密码错误!"}
	//ErrSignInFail 登录失败！
	ErrSignInFail = message.ResHead{RetCode: "0006", RetMsg: "登录失败！"}
)
