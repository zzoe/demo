package util

import (
	"strings"
)

//Add add
func Add(a, b int) int {
	return a + b
}

//Mul multiple
func Mul(a, b int) int {
	return a * b
}

func IsSpace(s string) bool {
	return strings.TrimSpace(s) == ""
}
