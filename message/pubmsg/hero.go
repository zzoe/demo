package pubmsg

import "gitee.com/zzoe/demo/message"

//Hero hero struct
type Hero struct {
	ID   int    `json:"id" msgpack:"mID"`
	Name string `json:"name" msgpack:"mName"`
}

//HeroGetAllArgs args
type HeroGetAllArgs struct {
}

//HeroGetAllData reply data
type HeroGetAllData struct {
	Heroes []Hero `json:"heroes" msgpack:"mHeroes"`
}

// HeroGetAllReply reply
type HeroGetAllReply struct {
	Head *message.ResHead `json:"head" msgpack:"mHead"`
	Body *HeroGetAllData  `json:"body" msgpack:"mBody"`
}
