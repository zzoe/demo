package pubmsg

import "gitee.com/zzoe/demo/message"

//SignArgs args for signup and signin
type SignArgs struct {
	Usercode string
	Password string
}

//SignData sign data
type SignData struct {
	Usercode  string
	SessionID string
}

//SignReply reply
type SignReply struct {
	Head *message.ResHead
	Body *SignData
}
