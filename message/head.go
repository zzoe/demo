package message

// ResHead return code and msg
type ResHead struct {
	RetCode string `json:"ret_code" msgpack:"mRetCode"`
	RetMsg  string `json:"ret_msg" msgpack:"mRetMsg"`
}
