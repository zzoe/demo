# demo

angular + rpcx + etcd + cockroachdb 尝试一个分布式Demo

## 启动cockroachdb

```shell
start cmd /k "D:\App\cockroach\cockroach.exe start --insecure --host=localhost"

start cmd /k "D:\App\cockroach\cockroach.exe start --insecure --store=node2 --host=localhost --port=26258 --http-port=8081 --join=localhost:26257"

start cmd /k "D:\App\cockroach\cockroach.exe start --insecure --store=node3 --host=localhost --port=26259 --http-port=8082 --join=localhost:26257"
```

## 启动etcd

```shell
start cmd /k "D:\App\etcd\etcd.exe"
```

## 启动rpc服务

```shell
go run -tags etcd server.go
```

## 启动client服务

[Client 说明](./client/web/README.md "Markdown")